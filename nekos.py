import requests, json, argparse, sys
from colorama import init, Style, Fore

init()
URL = "https://nekos.best/api/v2"
ENDPOINTS = {"k": "/kitsune", "n": "/neko", "w": "/waifu", "u": "/husbando"}
argcount = 0

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-k", action="store_const", const=1, help="kitsune")
    parser.add_argument("-n", action="store_const", const=1, help="neko")
    parser.add_argument("-w", action="store_const", const=1, help="waifu")
    parser.add_argument("-u", action="store_const", const=1, help="husbando")
    args = vars(parser.parse_args())

    # check for only 1 arg existing
    for i in args.values():
        try:
            argcount += i
        except TypeError:
            continue
    if argcount != 1:
        print("You can only use 1 option at a time")
        sys.exit(1)
    for i in args.keys():
        if args[i] == 1:
            chosen = i

    # send request
    res = requests.get(URL + ENDPOINTS[chosen]).json()
    res = res["results"][0]

    # pretty text
    print(f"{Style.BRIGHT}{Fore.LIGHTMAGENTA_EX}{ENDPOINTS[chosen]}{Style.RESET_ALL}")
    print(f"{Fore.LIGHTCYAN_EX}Artist:{Style.RESET_ALL}", res["artist_name"])
    print(f"{Fore.LIGHTCYAN_EX}Artist link:{Style.RESET_ALL}", res["artist_href"])
    print(f"{Fore.LIGHTCYAN_EX}Source:{Style.RESET_ALL}", res["source_url"])
    print(f"{Fore.LIGHTCYAN_EX}Picture:{Style.RESET_ALL}", res["url"])
    sys.exit(0)
