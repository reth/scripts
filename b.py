import re, argparse, random

NEWCONTENT, EMOJIBUFFER, EMOJIS = [], [], ["🔥","😂","💯","👌","🗿"]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("-e", action="store_true", help="add emojis")
    parser.add_argument("-b", action="store_true", help="convert b-only")
    parser.add_argument("-u", action="store_true", help="don't uppercase input")
    args = vars(parser.parse_args())
    
    # read file and convert to upper if needed
    with open(args["filename"]) as file:
        if args["u"]:
            content = [line.strip() for line in file.readlines()]
        else:
            content = [line.strip().upper() for line in file.readlines()]

    # create regex
    if args["b"]:
        regex = r'(B|b)|(Б|б)'
    else:
        regex = r'(B|D|P|b|d|p)|(Б|Д|П|б|д|п)'

    # convert to B's
    for line in content:
        NEWCONTENT.append(re.sub(regex,"🅱️",line))

    # emojify if needed
    if args["e"]:
        for line in NEWCONTENT:
            if line != "":
                combo = "".join(random.sample(EMOJIS, 3))
                EMOJIBUFFER.append(f"{line} {combo}")
            else: EMOJIBUFFER.append(line)
        NEWCONTENT = EMOJIBUFFER

    # save to file
    try:
        with open("b.out.txt","x") as out:
            out.write("\n".join(NEWCONTENT))
    except FileExistsError:
        with open("b.out.txt","w") as out:
            out.write("\n".join(NEWCONTENT))
